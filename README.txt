
Civil Rights CAPTCHA for Drupal
====================

The Civil Rights CAPTCHA module adds the Civil Rights CAPTCHA web
service to the CAPTCHA system.

This module provides a Swedish language CAPTCHA automatically if
the Locale module language is set to 'sv'.

For more information on what Civil Rights CAPTCHA is, please visit:
http://captcha.civilrightsdefenders.org

This module was based on the code from the Egglue_captcha module.

INSTALLATION
------------

1. Extract the Civil Rights CAPTCHA module to your local favorite
   modules directory (sites/all/modules).

2. Extract and Enable both the dependencies:
   CAPTCHA: http://drupal.org/project/captcha
   Libraries API: http://drupal.org/project/libraries

3. Download the Civil Rights Captcha Library:
   http://code.google.com/p/civil-rights-defenders-captcha/downloads/list
   And extract CRCphplib.php into /libraries/crc/

CONFIGURATION
-------------

4. Enable the Civil Rights CAPTCHA module in:
   admin/modules

5. Select Civil Rights CAPTCHA as the challenge type for your forms
   at the path: admin/config/people/captcha

CONTACT
-------

Current maintainers:
Douglas G Reith (dooug) - http://drupal.org/user/417693
